
package fr.limos.splitpdf;




import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.pdmodel.PDDocument;
/*
 import org.apache.pdfbox.cos.COSBase;
 import org.apache.pdfbox.cos.COSName;
 import org.apache.pdfbox.pdmodel.graphics.PDXObject;
 import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
 import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
 import org.apache.pdfbox.util.Matrix;
 import org.apache.pdfbox.contentstream.operator.DrawObject;
 import org.apache.pdfbox.contentstream.operator.Operator;
 */

// *****************  FT##  *****************
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;

// ***************** FIN FT##   ******************************


@Deprecated
public class SecondPagesHandler extends PDFStreamEngine
{
    private static String prefixOutputSubFolder = "c";
    private static int directoriesBasedNameIndex = 2;
    private static final String CSV_FILENAME_BEGINNING = "second_pages_mapping_"; 
    private static final String PROCESSED_CSV_FILENAME_BEGINNING = "processed_second_pages_mapping_"; 
 
    public static void main( String[] args ) throws IOException
    {
        if (args.length < 2) {
            System.out.println("2 arguments required: csvMappingFile pdfDirectory");
            System.out.println("The program can also read the content of a directory (not recursive) : csvMappingDirectory pdfDirectory");
            System.out.println("2 more arguments are available: the prefix for the name of the sub-directories containing the pdf files (default is 'c'), and the index of the element in the filenames that is used for the sub-directories names.");
        }
        else {
            String csvMappingFile = args[0];
            String pdfDirectory = args[1];
            if (args.length > 2) {
                prefixOutputSubFolder = args[2];
                if (args.length > 3) {
                    try {
                        directoriesBasedNameIndex = Integer.parseInt(args[3]);
                    } catch (Exception e) {
                        System.out.println("The fourth argument is an index (integer: 0 to size-1) of the element in the filenames that is used for the sub-directories names.");
                        return;
                    }
                }
                else {
                    System.out.println("A fourth argument is required: it is an index (integer: 0 to size-1) of the element in the filenames that is used for the sub-directories names.");
                    return;
                }
            }
            
//            String csvMappingFile = "C:/Users/cepedac/Documents/NetBeansProjects/Profan-papers/output";
//            String pdfDirectory = "C:/Users/cepedac/Documents/NetBeansProjects/Profan-papers/output";

            File input = new File(csvMappingFile);
            File directory = new File(pdfDirectory);
            if (!input.exists() || !directory.exists() || !directory.isDirectory()) {
                System.out.println("Check your arguments!");
                System.out.println("2 arguments required : csvMappingFile pdfDirectory");
                System.out.println("The program can also read the content of a directory (not recursive) : csvMappingDirectory pdfDirectory");
                System.out.println("2 more arguments are available: the prefix for the name of the sub-directories containing the pdf files (default is 'c'), and the index of the element in the filenames that is used for the sub-directories names.");
            }
            else {
                if (input.isFile())
                    processFile(input, pdfDirectory);
                else {
                    for (File file : input.listFiles()) {
                        processFile(file, pdfDirectory,true);
                    }
                }
                    
            }
            
        }
    }
    
    private static void processFile(File inputFile, String pdfDirectory) {
        processFile(inputFile, pdfDirectory, false);
    }
    private static void processFile(File inputFile, String pdfDirectory, boolean throughDirectory) {
        if (inputFile.getName().endsWith(".csv") && (!throughDirectory || inputFile.getName().startsWith(getCSV_FILENAME_BEGINNING()))) {
            BufferedReader br = null;
            String line ;
            String csvSplitBy = ";";
            String[] data ;
            try {
                br = new BufferedReader(new FileReader(inputFile));
                while ((line = br.readLine()) != null) {
                    data = line.split(csvSplitBy);
                    if (data.length == 2)
                        deleteFirstPagePdf(data[1], pdfDirectory);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        Files.move(inputFile.toPath(), 
                                new File(inputFile.getAbsolutePath().replace(getCSV_FILENAME_BEGINNING(), PROCESSED_CSV_FILENAME_BEGINNING)).toPath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
    private static void deleteFirstPagePdf(String filename, String pdfFolder) throws IOException {
        if (!pdfFolder.endsWith("/") || !pdfFolder.endsWith("\\"))
            pdfFolder += "/";
        
        String elementId = "";
        try {
            elementId = filename.split("-")[directoriesBasedNameIndex];
        } 
        catch (Exception e) {
            System.out.println("The fourth argument is an index (integer: 0 to size-1) of the element in the filenames that is used for the sub-directories names. Verify also that the separator in the filenames is '-'.");
            return;
        }
        String filePath = pdfFolder + prefixOutputSubFolder + elementId + "/" + filename + ".pdf";
        File file = new File(filePath);
            
        try (PDDocument document = PDDocument.load(file))
        {
            System.out.println( "************************");
            System.out.println("Processing file " + file.getName());

            if (document.getNumberOfPages() == 1) {
                document.close();
                file.delete();
            }
            else
                Tools.savePdfBlock(filename, 1, document.getNumberOfPages()-1, document, pdfFolder, prefixOutputSubFolder, false, null, null, null);

            System.out.println("End file " + file.getName());
            System.out.println("************************");
        }
    }

    /**
     * @return the CSV_FILENAME_BEGINNING
     */
    public static String getCSV_FILENAME_BEGINNING() {
        return CSV_FILENAME_BEGINNING;
    }
    
}
