
package fr.limos.splitpdf;


import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import java.util.*;
/*
 import org.apache.pdfbox.cos.COSBase;
 import org.apache.pdfbox.cos.COSName;
 import org.apache.pdfbox.pdmodel.graphics.PDXObject;
 import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
 import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
 import org.apache.pdfbox.util.Matrix;
 import org.apache.pdfbox.contentstream.operator.DrawObject;
 import org.apache.pdfbox.contentstream.operator.Operator;
 */

// *****************  FT##  *****************
import org.apache.pdfbox.rendering.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// ***************** FIN FT##   ******************************



public class SplitQRCodePages extends PDFStreamEngine
{
    
    private static List<String> dataNameInQRCode  = Arrays.asList("student", "exam", "class", "page");
    private static int directoriesBasedNameIndex = 2;
    private static String qrCodeTextRegex = "(\\d+-){3}\\d+";
    private static String qrCodeTextSeparator = "-";
    
    private static final int COPIE_PAGES_NM_MAX = 50;
    
    private static StringBuilder strBuilderAlerts;
 
    public static void main( String[] args ) throws IOException
    {
        
        if (args.length < 2) {
            System.out.println("2 arguments required: inputFile outputDirectory");
            System.out.println("The program can also read resursively the content of a directory (please, not '/'): inputDirectory outputDirectory");
            System.out.println("2 more arguments are available: a json array of strings (= data column names, default is: [\"student\", \"exam\", \"class\", \"page\"]), and index of the element in the previous array that will be used for grouping the split pdf files into directories (0 to size-1, default is: 2 (for 'class')).");
        }
        else {
            String inputPath = args[0];
            String outputFolder = args[1];
            if (args.length > 2) {
                if (Tools.isJSONValid(args[2],true)) {
                    JSONArray array = new JSONArray(args[2]);
                    try {
                        dataNameInQRCode = new ArrayList<String>();
                        for (Object obj : array.toList()) {
                            dataNameInQRCode.add(obj.toString());
                        }
                    } catch (Exception e) {
                        System.out.println("Problem for reading the third argument: it must be a json array of strings! (data column names)");
                        System.out.println("Example: [\"student\",\"exam\",\"class\", \"page\"]");
                        return;
                    }
                    if (args.length > 3) {
                        try {
                            directoriesBasedNameIndex = Integer.parseInt(args[3]);
                            if (directoriesBasedNameIndex < 0 && directoriesBasedNameIndex >= dataNameInQRCode.size()) {
                                System.out.println("The fourth argument is an index (integer: 0 to size-1) of the json array element. This element will be used for grouping the split pdf files into folders/directories.");
                                return;
                            }
                        } catch (NumberFormatException e) {
                            System.out.println("The fourth argument is an index (integer: 0 to size-1) of the json array element. This element will be used for grouping the split pdf files into folders/directories.");
                            return;
                        }
                    }
                    else {
                        System.out.println("A fourth argument is required: it is an index (integer: 0 to size-1) of the json array element. This element will be used for grouping the split pdf files into folders/directories.");
                        return;
                    }
                }
                else {
                    System.out.println("Problem for reading the third argument: it must be a json array of strings! (data column names)");
                    System.out.println("Example: [\"student\",\"exam\",\"class\", \"page\"]");
                    return;
                }
            }
                    
//            String inputPath = "C:\\Users\\cepedac\\Documents\\NetBeansProjects\\SplitPDF\\input";
//            String outputFolder = "C:\\Users\\cepedac\\Documents\\NetBeansProjects\\SplitPDF\\output";

            strBuilderAlerts = new StringBuilder();
            File input = new File(inputPath);
            if (!input.exists()) {
                System.out.println("Check your input path, you idiot! (Both directory and file are allowed)");
                return;
            }
            else {
                long timeBefore = Calendar.getInstance().getTimeInMillis();
                
                if (input.isFile())
                    processFile(input, outputFolder);
                else
                    processFolder(input, outputFolder);
                
                long duration = Calendar.getInstance().getTimeInMillis() - timeBefore;
                System.out.println("Duration = "+String.format("%02d h, %02d min, %02d s, %02d ms",
                        TimeUnit.MILLISECONDS.toHours(duration),
                        TimeUnit.MILLISECONDS.toMinutes(duration)-TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
                        TimeUnit.MILLISECONDS.toSeconds(duration)-TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)),
                        duration-TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(duration))));
            }
            
            String timeString = new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss").format(Calendar.getInstance().getTime());
            Tools.writeFile(strBuilderAlerts.toString(), outputFolder+"/alerts-"+timeString+".txt");
        }
    }
    
    private static void processFolder(File folder, String outputFolder) throws IOException {
        System.out.println( "***");
         System.out.println("Processing directory " + folder.getName());
        for (File file : folder.listFiles()) {
            if (file.isFile())
                processFile(file,outputFolder);
            else
                processFolder(file, outputFolder);
        }
        System.out.println("End directory " + folder.getName());
        System.out.println( "***");
    }
    
    private static void processFile(File pdf, String outputFolder) throws IOException {
        if (pdf.getName().endsWith(".pdf")) {
            StringBuilder invalidQRfound = new StringBuilder();
            Map<String,StringBuilder> logQRfoundMap = new HashMap<String,StringBuilder>();
//            PDDocument secondPagesPdf = new PDDocument();
//            StringBuilder secondPagesList = new StringBuilder();
//            int secondPagesCpt = 0;

            String filename;
            Integer first, last;
            StringBuilder strBuilder;
//            ArrayList<Integer> firstPagesList = new ArrayList<Integer>();     // utilisé lorsque le QR code était mis une seule fois, avant la copie.
//            ArrayList<String> filenamesList = new ArrayList<String>();       // utilisé lorsque le QR code était mis une seule fois, avant la copie.
            Map<String,Integer[]> pagesMap = new HashMap<String,Integer[]>();
            Integer[] pagesList;
            BufferedImage bimOriginal, bim, bimRight ;
            String decodedText ;
            String[] decodedValues; 
            JSONObject jsonObj;
            
            double scaleX = 0.65, scaleY = 0.65;
            AffineTransform at = new AffineTransform();
            at.scale(scaleX, scaleY);
            AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);

            try (PDDocument document = PDDocument.load(pdf))
            {
                System.out.println( "************************");
                 System.out.println("Processing file " + pdf.getName());

                PDFRenderer pdfRenderer = new PDFRenderer(document);

                int pageNum = 0, nbCopies = 0, sumContent = 0 ; 
                for( PDPage page : document.getPages() )
                {
                    // full image, top-left image, top-right image
                    bimOriginal = pdfRenderer.renderImageWithDPI(pageNum, 300, ImageType.GRAY);
                    bim = bimOriginal.getSubimage(0, 0, bimOriginal.getWidth()/3, bimOriginal.getHeight()/4-120);
                    bimRight = bimOriginal.getSubimage(2*bimOriginal.getWidth()/3, 0, bimOriginal.getWidth()/3, bimOriginal.getHeight()/4-120);
                    
                    /* Image processing !!! */
                    decodedText = Tools.decodeQRCode(bim);
                    if(decodedText == null) {
                        decodedText = Tools.decodeQRCode(bimRight);
                    }
                    if(decodedText == null) {
                        decodedText = processImageForQRCode(bim,scaleOp,scaleX,scaleY);
                    }
                    if(decodedText == null) {
                        decodedText = processImageForQRCode(bimRight,scaleOp,scaleX,scaleY);
                    }
                    if(decodedText == null) {
                        decodedText = Tools.decodeQRCode(bimOriginal);
                    }

//                    File outputfile = new File("output/img/image"+pageNum+".jpg");
//                    ImageIO.write(bim, "jpg", outputfile);
                    
                    
                    if(decodedText == null) {
                        System.out.println("Page "+(pageNum+1)+" : no QR Code found");
                        sumContent++;
                        
                        writeImage(outputFolder+"/content/"+pdf.getName()+"-p"+(pageNum+1)+"-noqrcode.jpg", pageNum, pdfRenderer);
                        strBuilderAlerts.append("Document ").append(pdf.getName()).append(" ; Pas de QR CODE sur page ").append(pageNum+1).append("\r\n");

                    } else {
                        System.out.println("Page "+(pageNum+1)+" : decoded QR Code text = " + decodedText);
                        nbCopies++;

                        
/* // Bout de code utilisé lorsque l'on lisait des QR codes contenant un texte au format JSON.
                        if (Tools.isJSONValid(decodedText,false)) {
                            jsonObj = new JSONObject(decodedText);
                            try {
                                for (int i = 0 ; i < dataNameInQRCode.size() ; i++) {
                                    decodedValues[i] = ""+ jsonObj.get(dataNameInQRCode.get(i));
                                }
                            }
                            catch (JSONException e) {
                                invalidQRfound.append(pageNum+1).append(";").append(decodedText).append("\r\n");
                            }
                        
                            if (!Tools.arrayHasAnEmptyString(decodedValues)) {
*/                      
                        if (Pattern.matches(qrCodeTextRegex,decodedText)) {
                            decodedValues = decodedText.split(qrCodeTextSeparator);
                            
                            if (decodedValues.length == dataNameInQRCode.size()) {
                                // on construit le nom du fichier correspondant au qr code lu => nom de la copie : student - exam - class
                                filename = "";
                                int cpt = 0 ;
                                for (String val : decodedValues) {
                                    filename += (val + "-");
                                    cpt++;
                                    if (cpt > 2)
                                        break;
                                }
                                filename = filename.substring(0, filename.length()-1);

//                                filenamesList.add(filename);     // utilisé lorsque le QR code était mis une seule fois, avant la copie.
    //                            firstPagesList.add(pageNum);     // utilisé lorsque le QR code était mis une seule fois, avant la copie.
                                
                                // on met le numero de page du document courant (=pageNum) dans le bon tableau de numeros de page (en face du nom de la copie)
                                int id = Integer.parseInt(decodedValues[0]);
                                pagesList = pagesMap.get(filename);
                                if (pagesList == null) {
                                    pagesList = new Integer[COPIE_PAGES_NM_MAX];
                                    pagesMap.put(filename, pagesList);
                                }
                                pagesList[Integer.parseInt(decodedValues[3])] = pageNum;    // rmq : tableau a pour index le numero de la page de copie (et, pour valeur, le no de la page du document)

                                // pour faire la liste des copies traitées : log
                                strBuilder = logQRfoundMap.get(decodedValues[directoriesBasedNameIndex]);
                                if  (strBuilder == null) {
                                    strBuilder = new StringBuilder();
                                    logQRfoundMap.put(decodedValues[directoriesBasedNameIndex], strBuilder);
                                }
                                if (strBuilder.indexOf(filename)<0)
                                    strBuilder.append(filename).append("\r\n");
                            }
                            else {
                                invalidQRfound.append(pageNum+1).append(";").append(decodedText).append("\r\n");
                                
                                writeImage(outputFolder+"/content/"+pdf.getName()+"-p"+(pageNum+1)+"-invalid.jpg", pageNum, pdfRenderer);
                            }
                        }
                        else {
                            if (decodedText.contains(";"))
                                decodedText = "\""+decodedText+"\"";
                            invalidQRfound.append(pageNum+1).append(";").append(decodedText).append("\r\n");
                            
                            writeImage(outputFolder+"/content/"+pdf.getName()+"-p"+(pageNum+1)+"-invalid.jpg", pageNum, pdfRenderer);
                        }
                    }

                    pageNum++;

                }
                
/* // Bout de code utilisé lorsque le QR code était mis une seule fois, avant la copie.   
                double moyenne = 0.0;
                if (nbCopies > 0)
                    moyenne = (double)sumContent / (double)nbCopies;
*/

                System.out.println( "************");
                 System.out.println("Number of copies: " + pagesMap.size());
                 
                 // création des copies avec les données récupérées auparavant
                if (pagesMap.size() > 0) {
/* // Bout de code utilisé lorsque le QR code était mis une seule fois, avant la copie.
                    if (firstPagesList.get(0) != 0) {
                        strBuilderAlerts.append("Document ").append(pdf.getName()).append(" ; pas lu de QR Code sur 1ere page").append("\r\n");
                    }
                    
                    for (int i = 0; i < firstPagesList.size()-1  ; i++) {
                        first=firstPagesList.get(i);
                        last=firstPagesList.get(i+1)-1;

                         if (last >= first+1) {
                             System.out.println("Save copy number "+(i+1)+" (page "+(first+1)+") : "+filenamesList.get(i));
                             Tools.savePdfBlock(filenamesList.get(i), first+1, last, document, outputFolder, ""+dataNameInQRCode.get(directoriesBasedNameIndex).charAt(0), true, strBuilderAlerts, pdf.getName(), pdfRenderer);

//                             secondPagesPdf.addPage(document.getPage(first+1));
//                             secondPagesList.append(++secondPagesCpt).append(";").append(filenamesList.get(i)).append("\r\n");
                         }
                         
                        if (last - first > (moyenne+1)) {
                            strBuilderAlerts.append("Document ").append(pdf.getName()).append(" ; copie page ").append(first+1).append(" : nb pages = ").append(last - first).append(" >").append(moyenne+1).append(" (").append(filenamesList.get(i)).append(")\r\n");
                        }
                    }
                    
                     // last document
                     first=firstPagesList.get(firstPagesList.size()-1);
                     last=pageNum-1;

                     if (last >= first+1) {
                         System.out.println("Save copy number "+firstPagesList.size()+" (page "+(first+1)+") : "+filenamesList.get(filenamesList.size()-1));
                         Tools.savePdfBlock(filenamesList.get(filenamesList.size()-1), first+1, last, document, outputFolder, ""+dataNameInQRCode.get(directoriesBasedNameIndex).charAt(0), true, strBuilderAlerts, pdf.getName(), pdfRenderer);

//                         secondPagesPdf.addPage(document.getPage(first+1));
//                         secondPagesList.append(++secondPagesCpt).append(";").append(filenamesList.get(filenamesList.size()-1)).append("\r\n");
                     }
                     
                    if (last - first > (moyenne+1)) {
                         strBuilderAlerts.append("Document ").append(pdf.getName()).append(" ; copie page ").append(first+1).append(" : nb pages = ").append(last - first).append(" >").append(moyenne+1).append(" (").append(filenamesList.get(filenamesList.size()-1)).append(")\r\n");
                    }
*/
                    
                    // on sauvegarde chaque copie dans un fichier pdf au nom adapté
                    Set<String> outputFilenames = pagesMap.keySet();
                    for (String outputFilename : outputFilenames) {
                        pagesList = pagesMap.get(outputFilename);
                        if (pagesList.length > 0) {
                            Tools.savePdfBlock(outputFilename, pagesList, document, outputFolder, ""+dataNameInQRCode.get(directoriesBasedNameIndex).charAt(0), strBuilderAlerts, pdf.getName());
                        }
                    }
                    
                } 
/* // Bout de code utilisé lorsque le QR code était mis une seule fois, avant la copie.
                else {
                    strBuilderAlerts.append("AUCUN QR CODE dans document : ").append(pdf.getName()).append("\r\n");
                }
*/
                
                writeLogs(logQRfoundMap, outputFolder);
//                saveSecondPagesPdf(secondPagesPdf, secondPagesList.toString(), outputFolder, pdf.getName());
                writeInvalidQRCodes(invalidQRfound.toString(), outputFolder, pdf.getName());

                System.out.println("End file " + pdf.getName());
                System.out.println("************************");
            }
        }
    }
    
    /**
     * Transforme l'image donnée, avec des moyens différents, pour trouver un qr code.
     * 
     * @param bim
     * @param scaleOp
     * @param scaleX
     * @param scaleY
     * @return
     * @throws IOException 
     */
    private static String processImageForQRCode(BufferedImage bim, AffineTransformOp scaleOp, double scaleX, double scaleY) throws IOException {
        String decodedText = null;
        BufferedImage bimFiltered, bimPosterized, after ;
        float ninth = 1.0f / 9.0f;
        float[] edgeKernel = new float[]{
            ninth, ninth, ninth,
            ninth, ninth, ninth,
            ninth, ninth, ninth
        };
        BufferedImageOp bimOp = new ConvolveOp(new Kernel(3, 3, edgeKernel));
        
        // Change la résolution de l'image (plus mauvaise résolution) pour gommer les imperfections
        if(decodedText == null && scaleOp != null) {
            after = scaleImage(bim, scaleOp, scaleX, scaleY);

//            File outputfile = new File("output/img/scaled-image"+pageNum+".jpg");
//            ImageIO.write(after, "jpg", outputfile);

            decodedText = Tools.decodeQRCode(after);
        }
        
        // Applique plusieurs fois un filtre qui, pour chaque pixel, calcule une moyenne des pixels voisins (cf. edgeKernel). Permet aussi de gommer les imperfections.
        // pour être sûr, on change aussi la résolution de l'image après coup.
        bimFiltered = bim;
        int cpt = 0;
        while (cpt < 20 && decodedText == null) {
            bimFiltered = bimOp.filter(bimFiltered, null);
            decodedText = Tools.decodeQRCode(bimFiltered);

            if(decodedText == null && scaleOp != null) {
                after = scaleImage(bimFiltered, scaleOp, scaleX, scaleY);
                decodedText = Tools.decodeQRCode(after);
            }
            
            cpt++;
        }
        
        
        // Applique plusieurs fois un filtre qui, pour chaque pixel, calcule une moyenne des pixels situés sur les coins (cf. edgeKernel). Permet de mieux dessiner les contours.
        // pour être sûr, on change aussi la résolution de l'image après coup.
        edgeKernel = new float[]{
            0.25f, 0.0f, 0.25f,
            0.0f, 0.0f, 0.0f,
            0.25f, 0.0f, 0.25f
        };
        bimOp = new ConvolveOp(new Kernel(3, 3, edgeKernel));
        bimFiltered = bim;
        cpt = 0;
        while (cpt < 15 && decodedText == null) {
            bimFiltered = bimOp.filter(bimFiltered, null);
            decodedText = Tools.decodeQRCode(bimFiltered);

            if(decodedText == null && scaleOp != null) {
                after = scaleImage(bimFiltered, scaleOp, scaleX, scaleY);
                decodedText = Tools.decodeQRCode(after);
            }
            
            cpt++;
        }
        
        return decodedText;
    }
    
    /**
     * Permet de changer la résolution d'une image donnée.
     * 
     * @param src
     * @param scaleOp
     * @param scaleX
     * @param scaleY
     * @return 
     */
    private static BufferedImage scaleImage(BufferedImage src, AffineTransformOp scaleOp, double scaleX, double scaleY) {
        if (src != null && scaleOp != null) {
            int w = src.getWidth();
            int h = src.getHeight();
            BufferedImage after = new BufferedImage((int)(scaleX*w), (int)(scaleY*h), BufferedImage.TYPE_BYTE_GRAY);
            after = scaleOp.filter(src, after);
            
            return after;
        }
        else
            return null;
        
    }
    
    private static void writeInvalidQRCodes(String invalidQRfound, String outputFolder, String inputFileName) {
        if (!outputFolder.endsWith("/") || !outputFolder.endsWith("\\"))
            outputFolder += "/";
        
        if (invalidQRfound != null && !invalidQRfound.isEmpty())
            Tools.writeFile(invalidQRfound, outputFolder + "invalid_codes_"+inputFileName+".csv");
    }
    
//    private static void saveSecondPagesPdf(PDDocument secondPagesPdf, String secondPagesList, String outputFolder, String inputFileName) {
//        if (!outputFolder.endsWith("/") || !outputFolder.endsWith("\\"))
//            outputFolder += "/";
//        
//        if (secondPagesPdf != null && secondPagesPdf.getNumberOfPages() > 0) {
//            try {
//                secondPagesPdf.save(outputFolder + "second_pages_"+inputFileName+".pdf");
//                secondPagesPdf.close();
//            }
//            catch (IOException e) {
//                System.out.println("Problem to save the file second_pages.pdf");
//            }
//        }
//        
//        if (secondPagesList != null && !secondPagesList.isEmpty())
//            Tools.writeFile(secondPagesList, outputFolder + SecondPagesHandler.getCSV_FILENAME_BEGINNING()+inputFileName+".csv");
//    }
    
//    private static void saveContentPdf(PDDocument contentPdf, String outputFolder, String inputFileName) {
//        if (!outputFolder.endsWith("/") || !outputFolder.endsWith("\\"))
//            outputFolder += "/";
//        
//        if (contentPdf != null && contentPdf.getNumberOfPages() > 0) {
//            try {
//                contentPdf.save(outputFolder + "content_"+inputFileName+".pdf");
//                contentPdf.close();
//            }
//            catch (IOException e) {
//                System.out.println("Problem to save the file content.pdf");
//            }
//        }
//    }
    
    private static void writeLogs(Map<String,StringBuilder> logQRfoundMap, String outputFolder) {        
        if (!outputFolder.endsWith("/") || !outputFolder.endsWith("\\"))
            outputFolder += "/";
        
        Set<String> classesList = logQRfoundMap.keySet();
        
        for (String classId : classesList) {
            Tools.writeFile(logQRfoundMap.get(classId).toString(), outputFolder + "c" + classId + "/by_"+dataNameInQRCode.get(directoriesBasedNameIndex)+".log");
        }
    }
    
    
    private static void writeImage(String filename, int pageNum, PDFRenderer pdfRenderer) throws IOException {
        File outputImage = new File(filename);
        outputImage.mkdirs();
        ImageIO.write(pdfRenderer.renderImageWithDPI(pageNum, 72, ImageType.GRAY), "jpg", outputImage);
    }


}
