
package fr.limos.splitpdf;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

@Deprecated
public class QRCodeGenerator {
//    private static final String QR_CODE_IMAGE_PATH = "./generated/MyQRCode.png";
    
    private static void generateQRCodeImage(String text, int width, int height, String filePath)
    throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
        
        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
    }
    
    public static void main(String[] args) {
        try {
            String QR_CODE_IMAGE_PATH = "./generated/copie-";
            for (int i=1; i<=5; i++) {
                System.out.println("****  Generation copie --> " + i);
            generateQRCodeImage("code-eleve-"+i, 350, 350, QR_CODE_IMAGE_PATH+i+".png");
                
            }
        } catch (WriterException e) {
            System.out.println("Could not generate QR Code, WriterException :: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Could not generate QR Code, IOException :: " + e.getMessage());
        }
        
//        InputStream in = null;
//        try {
//            in = new FileInputStream("C:\\Users\\cepedac\\Documents\\NetBeansProjects\\SplitPDF\\output\\img\\image4.jpg");
//            BufferedImage bimg = ImageIO.read(in);
//            convertImgToPDF(bimg, "C:\\Users\\cepedac\\Documents\\NetBeansProjects\\SplitPDF\\output\\img\\image4.pdf");
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(QRCodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(QRCodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                if (in != null)
//                    in.close();
//            } catch (IOException ex) {
//                Logger.getLogger(QRCodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }
    
    public static void convertImgToPDF(BufferedImage image, String outputFileName) throws IOException {
        try (PDDocument document = new PDDocument()) {
            PDPage page = new PDPage(new PDRectangle(image.getWidth(), image.getHeight()));
            document.addPage(page);
            
            PDImageXObject pageImageXObject = LosslessFactory.createFromImage(document, image);
            try (PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
                contentStream.drawImage(pageImageXObject, 0, 0);
            }
            document.save(outputFileName);
        }
    }
}
