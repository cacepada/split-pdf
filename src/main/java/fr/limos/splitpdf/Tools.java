/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.limos.splitpdf;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author cepedac
 */
public class Tools {
    
    public static void savePdfBlock(String pdfname, Integer[] pagesNumber, PDDocument documentSource, String outputFolder, String prefixOutputSubFolder, StringBuilder logBuilder, String originalPdfname) throws IOException {
        if (!outputFolder.endsWith("/") || !outputFolder.endsWith("\\"))
            outputFolder += "/";

        String classId = pdfname.substring(pdfname.lastIndexOf("-")+1);
        File f = new File (outputFolder + prefixOutputSubFolder + classId);
        f.mkdirs();
        
        PDPage page ;
        String filename;
        boolean isFirstPage = true; int firstPage = -1 ;
        try (PDDocument docToSave = new PDDocument()) {
            for (Integer i: pagesNumber) {
                if (i != null) {
                    page = documentSource.getPage(i);
                    docToSave.addPage(page);
                    
                    if (isFirstPage) {
                        firstPage = i;
                        isFirstPage = false;
                    }
                }
            }
            
            filename = outputFolder + prefixOutputSubFolder + classId + "/" + pdfname + ".pdf";
            if (new File(filename).exists()) {
                if (logBuilder != null) {
                    logBuilder.append("Document ").append(originalPdfname).append(" ; copie page ").append(firstPage+1).append(" : copie deja existante (non ecrasee) = ").append(pdfname).append("\r\n");
                }
            }
            else
                docToSave.save(filename);
            docToSave.close();
        }
        catch (IOException e) {
            System.out.println("Problem to save the file " + pdfname+ ".pdf");
        }
    }
    
    @Deprecated
    public static void savePdfBlock(String pdfname, Integer firstpage, Integer lastpage, PDDocument documentSource, String outputFolder, String prefixOutputSubFolder, boolean saveContent, StringBuilder logBuilder, String originalPdfname, PDFRenderer originalPdfRenderer) throws IOException {
        if (!outputFolder.endsWith("/") || !outputFolder.endsWith("\\"))
            outputFolder += "/";

        String classId = pdfname.substring(pdfname.lastIndexOf("-")+1);
        File f = new File (outputFolder + prefixOutputSubFolder + classId);
        f.mkdirs();
        
        PDPage page ;
        String filename;
        File outputImage ;
        try (PDDocument docToSave = new PDDocument()) {
            for (int i =firstpage; i <=lastpage; i++) {
                page = documentSource.getPage(i);
                if (saveContent && originalPdfRenderer != null) {
                    outputImage = new File(outputFolder+"content/"+originalPdfname+"-p"+(i+1)+".jpg");
                    outputImage.mkdirs();
                    ImageIO.write(originalPdfRenderer.renderImageWithDPI(i, 72, ImageType.GRAY), "jpg", outputImage);
                }
                docToSave.addPage(page);
            }
            
            filename = outputFolder + prefixOutputSubFolder + classId + "/" + pdfname + ".pdf";
            if (new File(filename).exists()) {
                if (logBuilder != null) {
                    logBuilder.append("Document ").append(originalPdfname).append(" ; copie page ").append(firstpage+1).append(" : copie deja existante (non ecrasee) = ").append(pdfname).append("\r\n");
                }
            }
            else
                docToSave.save(filename);
            docToSave.close();
        }
        catch (IOException e) {
            System.out.println("Problem to save the file " + pdfname+ ".pdf");
        }
    }
    
    public static String decodeQRCode(BufferedImage bufferedImage) throws IOException {
        
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        
        Map<DecodeHintType, Object> hintMap = new HashMap<DecodeHintType, Object>();
        Collection<BarcodeFormat> list = new ArrayList<BarcodeFormat>();
        list.add(BarcodeFormat.QR_CODE);
        hintMap.put(DecodeHintType.POSSIBLE_FORMATS, list);
        hintMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        
        try {
            Result result = new MultiFormatReader().decode(bitmap,hintMap);
            return result.getText();
        } catch (NotFoundException e) {
            return null;
        }
    }
    
    
    public static void writeFile(String content, String path) {
        File f = new File (path);
        f.getParentFile().mkdirs();
        try (FileWriter fw = new FileWriter(f)) {
            fw.write(content);
            fw.flush();
            fw.close();
        }
        catch (IOException exception) {
            System.out.println ("Erreur lors de l'enregistrement du fichier "+f.getName()+" : " + exception.getMessage());
        }
    }
    
    
    /**
     * Not used anymore: vérifie que le texte donné est un objet/tableau json.
     * 
     * @param test
     * @param isArray
     * @return 
     */
    public static boolean isJSONValid(String test, boolean isArray) {
        try {
            if (!isArray)
                new JSONObject(test);
            else
                new JSONArray(test);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Not used anymore: vérifie si un des éléments récupérés dans le tableau est vide.
     * 
     * @param test
     * @param isArray
     * @return 
     */
    public static boolean arrayHasAnEmptyString(String[] array) {
        boolean hasEmptyString = false ;
        
        for (String element : array) {
            if (element == null || element.isEmpty()) {
                hasEmptyString = true ;
                break ;
            }
        }
        
        return hasEmptyString ;
    }
    
}
